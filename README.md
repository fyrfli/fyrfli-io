# fyrfli.io

Landing page for [fyrfli.io](https://fyrfli.io) and [camille.fyrfli.io](https://camille.fyrfli.io) and is a work in progress. Stay tuned for updates.

If you want to track the project, you can see the [project page on my Gitlab](https://gitlab.fyrfli.io/camille/camille.fyrfli.io).

## Updates

- February 17, 2022 02:53 - made design responsive